# Chapter05 참조 타입

## 5.1 데이터 타입 분류

기본 타입
- 정수, 실수, 문자, 논리 리터럴을 저장하는 타입

참조 타입
- 객체(Object)의 번지를 참조하는 타입으로 배열, 열거, 클래스, 인터페이스 타입을 말함

-> 기본 타입과 참조 타입의 차이점은 **저장되는 값이 무엇이냐** 이다.

![Alt text](DataType.png)

기본 타입은 선언된 변수에 실제 값을 그 안에 저장하지만, 참조 타입은 선언된 변수는 메모리의 번지를 값으로 가지고 번지를 통해 객체를 참조하는 것이다. 즉 객체에 실제로 저장될 데이터가 존재한다.

![Alt text](참조.PNG)

<br/>
<br/>

## 5.2 메모리 사용 영역

JVM은 운영체제에서 할당받은 메모리 영역(Runtime Data Area)을 다음과 같이 세부 영역으로 구분해서 사용한다.

![Alt text](메모리영역.PNG)

메소드 영역
- JVM이 시작할 때 생성되고 모든 스레드가 공유하는 영역
- 로딩된 클래스 바이트 코드 내용을 분석 후 저장

힙(Heap) 영역
- 객체와 배열이 생성되는 영역으로 객체와 배열 저장
- 사용되지 않는 객체는 Garbage Collector 가 자동 제거

JVM 스택(Stack) 영역
- 각 스레드마다 하나씩 존재하며 스레드가 시작될 때 할당
- 메소드를 호출할 때 마다 Frame을 추가(push)
- 메소드가 종료되면 Frame 제거(pop)

<br/>
<br/>

## 5.3 참조 변수의 ==, != 연산

![Alt text](참조2.PNG)

```java
refVar1 == refVar2  //false
refVar1 != refVar2  //true

refVar2 == refVar3 //true
refVar2 != refVar3 //false
```

<br/>
<br/>

## 5.4 null과 NullPointerException

참조 타입 변수는 힙 영역의 객체를 참조하지 않는다는 뜻으로 null 값을 가질 수 있다. 초기값으로 사용 가능하고, null로 초기화된 참조 변수는 **스택 영역**에 생성된다.

![Alt text](null.PNG)

NullPointerException 은 참조 변수가 null값을 가지고 있을 때 객체의 필드나 메소드를 사용하려고 했을 때 발생한다.

```java
int[] intArray = null;
intArray[0] = 10;  // NullPointerException
```

<br/>
<br/>

## 5.5 String 타입

String 타입은 자바에서 문자열을 저장하기 위한 타입이다.

```java
String name;
name = "신용권";
String hobby = "자바";
```
정확히 말하면 문자열이 직접 변수에 저장되는 것이 아니라, 문자열은 String 객체로 생성되고 변수는 String 객체를 참조한다. 

![Alt text](String.PNG)

문자열 리터럴이 동일하다면 String 객체를 공유한다.

```java
String name1 = "신용권"
String name2 = "신용권"
```

![Alt text](String2.PNG)

new 연산자를 이용하여 String 객체를 생성할 경우 힙 영역에 새로운 String 객체를 생성한 뒤 번지를 리턴한다.

```java
String name1 = new String("신용권");
String name2 = new String("신용권");
```
![Alt text](String3.PNG)

참조를 잃은 String 객체는 JVM이 쓰레기 객체로 취급하고 쓰레기 수집기(Gabage Collector)를 구동시켜 메모리에서 자동 제거한다.

<br/>
<br/>

## 5.6 배열 타입

**배열**
- 같은 타입의 데이터를 연속된 공간에 나열시키고, 각 데이터에 인덱스(index)를 부여해 놓은 자료구조

```java
타입[] 변수;
int [] intArray;

타입 변수[];
double doubleArray[];

타입[] 변수 = null; // null 값으로 초기화
```

<br/>

**값 목록으로 배열 생성**

```java
데이터타입[] 변수 = {값0, 값1, 값2, 값3, ...};

데이터타입[] 변수;
변수 = new 타입[]{값0, 값1, 값2, 값3, ...};
```

![Alt text](배열.PNG)

<br/>

**new 연산자로 배열 생성**
- 배열 생성 시 값 목록을 가지고 있지 않음
- 값들을 저장할 배열을 미리 생성하고 싶을 경우 사용

```java
타입[] 변수 = new 타입[길이];

타입[] 변수 = null;
변수 = new 타입[길이];
```

![Alt text](배열2.PNG)

<br/>

**배열 길이**
- 배열에 저장할 수 있는 전체 항목 수

코드에서 배열의 길이를 얻으려면 다음과 같이 배열 객체의 length 필드(객체 내부의 데이터)를 읽으면 된다.

```java
배열변수.length;
```

<br/>

**커맨드 라인 입력**

"java 클래스"로 뒤에 공백으로 구분된 문자열을 목록을 두고 실행하면, 문자열 목록으로 구성된 String[] 배열이 생성되고 main() 메소드를 호출할 때 매개값으로 전달된다.

![Alt text](커멘드.PNG)

```java
public class MainStringArrayArgument {
    public static void main(String[] args) {
        if(args.length != 2){
            System.exit(0);  // 프로그램 강제 종료
        }
        String strnum1 = args[0];
        String strnum2 = args[1];

        int num1 = Integer.parseInt(strnum1);  // 문자열을 정수로 변환
        int num2 = Integer.parseInt(strnum2);

        int result = num1 + num2;
        System.out.println(num1 + num2);
    }
}
```

![Alt text](커멘드2.PNG)

<br/>

**다차원 배열**

자바는 가로 인덱스와 세로 인덱스를 사용하여 중첩 배열방식으로 구현한다.

```java
int[][] scores = new int[2][3];
```

![Alt text](다차원배열.PNG)

<br/>

**객체를 참조하는 배열**

기본 타입 배열은 각 항목에 직접 값을 갖고 있지만, 참조 타입 배열은 각 항목에 객체의 번지를 가지고 있다. 

String은 클래스 타입이므로 String[] 배열은 각 항목에 문자열이 아니라, String 객체의 주소를 가지고 있다.

```java
String[] strArray = new String[3];
strArray[0] = "Java";
strArray[1] = "C++";
strArray[2] = "#";
```
![Alt text](객체배열.PNG)

<br/>

**배열 복사**

배열은 한 번 생성하면 크기 변경이 불가하므로 더 많은 저장 공간이 필요하면 보다 큰 배열을 새로 만들고 이전 배열로부터 항목 값들을 복사해야한다. 복사하려면 for문을 사용하거나 System.arraycopy() 메소드를 사용한다.

```java
public class ArrayCopyByForExample {
    public static void main(String[] args) {
        int[] oldIntArray = {1, 2, 3};
        int[] newIntArray = new int[5];
        
        for(int i = 0; i<oldIntArray.length; i++){
            newIntArray[i] = oldIntArray[i];
        }
        
        for(int i = 0; i < newIntArray.length; i++){
            System.out.println(newIntArray[i]);
        }
    }
}

```

System.arraycopy()를 호출하는 방법은 다음과 같다.

```java
System.arraycopy(oldIntArray, 0, newIntArray, 0, oldArray.length);
```

<br/>

**향상된 for문**

자바5부터 배열 및 컬렉션 객체를 좀 더 쉽게 처리할 목적으로 향상된 for문을 제공하는데 반복 실행을 하기 위한 카운터 변수와 증감식을 사용하지 않는다. 배열 및 컬렉션 항목의 개수만큼 반복하고, 자동적으로 for을 빠져나간다.

![Alt text](for.PNG)

```java
int[] scores = {95, 71, 84, 93, 87};

int sum = 0;
for(int score:socres){
  sum = sum + score;
}
```

<br/>
<br/>

## 5.7 열거 타입

**열거 타입**
- 한정된 값만을 갖는 데이터 타입

**열거 타입 선언**
- 열거 타입 이름으로 소스 파일(.java)을 생성
- 첫 문자를 대문자로 나머지는 소문자로 구성
- 파일 이름과 동일한 이름으로 다음과 같이 선언
- 한정된 값인 열거 상수 정의
  - 열거 상수 이름은 관례적으로 모두 대문자로 작성
  - 다른 단어가 결합된 이름일 경우 밑줄(_)로 연결
  

```java
public enum 열거타입이름 {...}

public enum Week{MONDAY, TUESDAY ...}
```

**열거 타입 변수**

```java
열거타입 변수;

Week today;
Week reservationDay;
```

**열거 상수 값 저장**
- 열거 타입 변수값은 열거 상수 중 하나

```java
열거 타입 변수 = 열거타입.열거상수;

Week today = Week.SUNDAY;

today == Week.SUNDAY  //true
```

**열거 객체의 메소드**
- 열거 객체는 열거 상수의 문자열을 내부 데이터로 가지고 있음
- 열거 타입은 컴파일 시 java.lang.Enum 클래스 자동 상속
  - 열거 객체는 java.lang.Enum 클래스의 메소드 사용 가능

| 리턴 타입 | 메소드(매개 변수) | 설명 |
| :-------- | :-------- | :-------- |
| String | name() | 열거 객체의 문자열 티런 |
| int | ordinal() | 열거 객체의 순번(0부터 시작) 리턴 |
| int | compareTo() | 열거 객체 비교해서 순번 차이 리턴 |
| 열거 타입 | valueOf(String name) | 주어진 문자열의 열거 객체 리턴 |
| 열거 배열 | values() | 모든 열거 객체들을 배열로 리턴 |