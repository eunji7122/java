# Chapter03 연산자

## 3.1 연산자와 연산식?

프로그램에서 데이터를 처리하여 결과를 산출하는 것을 연산(operation)이라고 한다. 연산에 사용되는 표시나 기호를 연산자(operator)라고 하고, 연산되는 데이터는 피연산자(operand)라고 한다. 연산자와 피연산자를 이용하여 연산의 과정을 기술한 것을 연산식(expression)이라고 부른다.

| 연산자 종류 | 연산자 | 피연산자 수 | 산출값 | 기능 설명 |
| :-------- | :-------- | :-------- | :-------- | :-------- |
| 산술 | +, -, *, /, % | 이항 | 숫자 | 사칙연산 및 나머지 계산 |
| 부호 | +, - | 단항 | 숫자 | 음수와 양수의 부호 |
| 문자열 | + | 이항 | 문자열 | 두 문자열을 연결 |
| 대입 | =, +=, -= 등 | 이항 | 다양 | 우변의 값을 좌변의 변수에 대입 |
| 증감 | ++, -- | 단항 | 숫자 | 1 만큼 증가/감소 |
| 비교 | ==, !=, > 등 | 이항 | boolean | 값의 비교 |
| 논리 | !, &, && 등 | 단항/이항 | boolean | 논리적 NOT, AND, OR 연산 |
| 조건 | (조건식) ? A:B | 삼항 | 다양 | 조건식에 따라 A 또는 B 중 하나 선택 |
| 비트 | ~, &, ^  =등 | 단항/이항 | 숫자/boolean | 비트 NOT, AND, OR, XOR 연산 |
| 쉬프트 | >>, <<, >>> | 이항 | 숫자 | 비트를 좌측/우측으로 밀어서 이동 |

- 연산자는 필요로 하는 피연산자의 수에 따라 단항, 이항, 삼항 연산자로 구분된다.
  ```java
  단항 연산자: ++x;
  이항 연산자: x + y;
  삼항 연산자: (sum>90) ? "A" : "B";
  ```

- 연산식을 반드시 하나의 값을 산출한다.
  ```java
  int result = x + y;
  ```

- 연산식은 다른 연산식의 피 연산자 위치에도 올 수 있다.
  ```java
  boolean result = (x+y) < 5;
  ```

<br/>

## 3.2 연산의 방향과 우선순위

![Alt text](연산자우선순위.PNG)

연산의 방향과 우선순위를 정리하면 다음과 같다.

```
1. 단항, 이항, 삼항 연산자 순으로 우선순위를 가진다.
2. 산술, 비교, 논리, 대입 연산자 순으로 우선순위를 가진다.
3. 단항과 대입 연산자를 제외한 모든 연산의 방향은 왼쪽에서 오른쪽이다.
4. 복잡한 연산식에는 괄호()를 사용해서 우선순위를 정해준다.
```

<br/>

## 3.3 오버플로우 탐지

산술 연산을 할 때 주의할 점은 연산 후의 산출값이 산출 타입으로 충분히 표현 가능한지 살펴봐야 한다. 산출 타입으로 표현할 수 없는 값이 산출되었을 경우, 오버플로우가 발생하고 쓰레기 값을 얻을 수 있기 때문이다.

```java
int x = 1000000;
int y = 1000000;
int z = x * y; // (x) 범위 초과
 
long z2 = x * y; // (o)
```

### **정확한 계산은 정수 사용**

정확하게 계산해야 할 때는 실수 타입을 사용하지 않는 것이 좋다. 

```java
int apple = 1;
double pieceUnit = 0.1;
int number = 7;

double result = apple - number*pieceUnit; // 0.2999999993
```

위 코드의 result 변수의 값은 0.299999993이 되어 정확히 0.3이 아니다. 이것은 이진 포맷의 기수를 사용하는 부동소수점 타입(float, double)은 0.1을 정확히 표현할 수 없어 근사치로 처리하기 때문이다. 정확히 계산하려면 정수 연산으로 변경해서 계산해야 한다.

<br/>

## 3.4 NaN과 Infinity 연산

/ 또는 % 연산자를 사용할 때 주의할 점이 있다. 좌측 피연산자가 정수 타입인 경우 나누는 수인 우측 피연산자는 0을 사용할 수 없다. 실행 시 ArithmeticException(예외) 가 발생한다.

```java
5 / 0 // ArithmeticException 예외 발생
5 % 0 // ArithmeticException 예외 발생
```

그러나 실수 타입인 0.0 또는 0.0f로 나누면 ArithmeticException 이 발생하지 않고, / 연산의 결과는 Infinity (무한대) 값을 가지며, % 연산의 결과는 NaN(Not a Number)을 가진다.

```java
5 / 0.0 // Infinity
5 % 0.0 // NaN
```

프로그램 코드에서 / 와 % 연산의 결과가 Infinity 또는 NaN인지 확인하려면 Double.inInfinite()와 Double.isNaN() 메소드를 이용하면 된다. 이 메소드들은 값이 Infinity 혹은 NaN 이라면 true를 리턴하고 아닐 경우는 false을 리턴한다.

```java
int x = 5;
double y = 0.0;

double z = x / y;

System.out.println(Double.isInfinite(z)); // true
System.out.println(Double.isNaN(z)); // false

System.out.println(z+2); // Infinity
```

### **입력값의 NaN 검사**

부동소수점(실수)을 입력받을 때는 반드시 NaN 검사를 해야한다.

```java
String userInput = "NaN";
double val = Double.valueOf(userInput); // 입력값을 double 타입으로 변환

double currentBalance = 10000.0;

currentBalance += val;
System.out.println(currentBalance); // NaN
```

NaN과 더하기 연산을 수행하면 currentBalance는 NaN이 저장되어 원래 데이터가 없어진다. 그렇기 때문에 문자열을 입력받을 때에는 반드시 "NaN"인지를 조사하고 만약 "NaN"이라면 NaN과 산술 연산을 수행해서는 안된다.