# Chapter04 조건문과 반복문

## 4.1 제어문

![Alt text](자바제어문.png)

## 4.2 조건문(if문, switch문)

### **4.2.1 if문**

if문은 조건식의 결과에 따라 블록 실행 여부가 결정된다.

![Alt text](if.png)

조건식에는 true 또는 false 값을 산출할 수 있는 연산식이나, boolean 변수가 올 수 있다. 조건식이 true이면 블록을 실행하고 false이면 블록을 실행하지 않는다.

### **4.2.2 if-else문**

if문은 else 블록과 함께 사용되어 조건식의 결과에 따라 실행 블록을 선택한다. if문의 조건식이 true이면 if문의 블록이 실행, false이면 else 블록이 실행된다.

![Alt text](if-else.png)

### **4.2.3 if-else if-else문**

조건문이 여러 개인 if문도 있다. 처음 if문의 조건식이 false일 경우 다른 조건식의 결과에 따라 실행 블록을 선택할 수 있는데, if 블록의 끝에 else if문을 붙이면 된다. 여러 개의 조건식 중 true가 되는 블록만 실행하고 모든 조건식이 false일 경우 else 블록을 실행하고 if문 벗어나게 된다.

![Alt text](else.PNG)

```java
int num = (int)(Math.random()*3 + 1); // 번호 하나 뽑기

if(num == 1){
  System.out.println("1");
} else if(num == 2){
  System.out.println("2");
} else {
  System.out.println("3");
}
```

### **4.2.4 중첩 if문**

if문의 블록 내부에는 또 다른 if문을 사용할 수 있다. 이것을 중첩 if문이라 부르는데, 중첩의 단계는 제한이 없기 때문에 실행 흐름을 잘 판단해서 작성하면 된다. if문, switch, for문, while문, do-while문 등 서로 중첩시킬 수 있다.

![Alt text](중첩if.PNG)

```java
if(score >= 90){
  if(score >= 95){
    grade = "A+";
  } else{
    grade = "A";
  }
} else{
  if(score >= 85){
    grade = "B+";
  } else{
    grade = "B";
  }
}
```

### **4.2.5 switch문**

switch문은 if문과 마찬가지로 조건 제어문이다. 하지만 switch문은 변수가 어떤 값을 갖느냐에 따라 실행문이 선택된다. 변수의 값에 따라서 실행되므로 같은 기능의 if문보다 코드가 간결하다.

![Alt text](switch.PNG)

```java
switch(num){
  case 1:
    System.out.println("1");
    break;
  case 2:
    System.out.println("2");
    break;
  default:
    System.out.println("3");
}
```

<br/>

## 4.3 반복문(for문, while문, do-while문)

반복문의 종류에는 for문, while문, do-while문이 있다. 보통 for문은 반복 횟수를 프로그래머가 알고 있을 때 주로 사용하고, while문은 조건에 따라 반복할 때 사용한다. while문과 do-while문의 차이점은 검사를 하고 실행하느냐, 실행하고 검사를 하느냐의 차이일 뿐 동작 원리는 같다.

### **4.3.1 for문**

for문이 실행될 때 초기화식이 먼저 실행되어 초기화를 진행한 뒤 조건식을 평가하여 true일 경우 실행문을 실행하고 아닐 경우 증감식을 실행시키고 다시 조건식을 평하게 된다. 조건식의 범위를 벗어나거나 false일 경우 for문을 빠져나오게 된다.

![Alt text](for문.PNG)

```java
int sum = 0;
for(int i = 1; i <= 100; i++){
  sum += i;
}
System.out.println("1~100 합: " + sum); // 5050
```

여기서 주의할 점은 위의 코드에서 for문 블록 밖에서의 변수 i는 사용되지 못한다는 것이다. 한 블록 내에서 선언한 변수는 블록 내에서만 사용할 수 있기 때문이다. 만약 변수 i를 계속해서 사용해야 한다면 int sum=0; 밑에 선언해주면 된다.

또 하나 주의할 점은 초기화식에서 루프 카운트 변수를 선언할 때 부동소수점 타입을 사용하지 말아야 한다. 0.1은 float 타입으로 정확하게 표현할 수 없기 때문에 x에 더해지는 실제값은 0.1보다 약간 크다. 결국 루프는 9번만 실행된다.

```java
for(float x = 0.1f; x <= 1.0f; x+=0.1f){
  System.out.println(x);
}
```

for문은 또 다른 for문을 내포할 수 있는데, 이것을 중첩된 for문이라고 한다. 이 경우 바깥쪽 for문이 한 번 실행될 때마다 중첩된 for문은 지정된 횟수만큼 반복해서 돌다가 다시 바깥쪽 for문으로 돌아간다.

```java
// 구구단 예제
for(int m = 2; m <=9; m++){
  System.out.println("***" + m + "단 ***");
  for(int n = 1; n <= 9; n++){
    System.out.println(m + " x " + n + " = " + (m*n));
  }
}
```

### **4.2.3 while문**

for문이 정해진 횟수만큼 반복한다면, while문은 조건식이 true일 경우에 계속해서 반복한다. 조건식에는 비교 또는 논리 연산식이 주로 오는데, 조건식이 false가 되면 반복 행위를 멈추고 while문을 종료한다.

![Alt text](while문.png)

```java
int i = 1;
while (i <= 10){
  System.out.println(i);
  i++;
}
```

다음 예제 코드는 키보드로부터 입력된 키 코드를 리턴하는 System.in.read() 메소드를 이용한 코드이다.

```java
public class WhilekeyControlExample{
  public static void main(String[] args) throws Exception{
    boolean run = true;
    int speed = 0;
    int keyCode = 0;

    while(run){
      if(keyCode !=13 ** keyCode != 10){ // Enter키가 입력되면 캐리지티런(13)과 라인피드(10)이 입력되므로 이 값을 제외
        System.out.println("-------------------------");
        System.out.println("1.증속 | 2. 감속 | 3. 중지");
        System.out.println("-------------------------");
        System.out.println("선택: ");
      }
      keyCode = System.in.read(); // 키보드의 키 코드를 읽음

      if(keyCode == 49){ // 1을 읽었을 경우
        speed++;
        System.out.println("현재 속도= " + speed);
      } else if (keyCode == 50) { // 2를 읽었을 경우
        speed--;
        System.out.println("현재 속도= " + speed);
      } else if (keyCode == 51){ // while문을 종료하기 위해 run변수에 false 저장
        run false;
      }
    }
    System.out.println("프로그램 종료");
  }
}
```

### **4.3.3 do-while문**

do-while과 while의 차이는 조건식을 언제 판단하느냐의 순서가 다를 뿐 조건식에 의해 반복 실행한다는 점에서는 while문과 동일하다.

while문은 시작할 때부터 조건식을 검사하여 블록 내부를 실행할지 결정하지만 경우에 따라서는 블록 내부의 실행문을 우선 실행시키고 실행 결과에 따라서 반복 실행을 계속할지 결정하는 경우도 발생한다. 이럴 때 사용하는 것이 do-while문이다.

다음 예제는 콘솔에 입력된 문자열을 한 번에 읽을 수 있는 nextLine() 메소드르 이용한 코드이다.

```java
import java.util.Scanner;

public class DowhileExample{
  public static void main(String[] args){
    System.out.println("메세지를 입력하세요.");
    System.out.println("시스템을 종료하려면 q를 입력하세요");

    Scanner sc = new Scanner(System.in);
    String inputString;

    do{
      System.out.println(">");
      inputString = sc.nextLine();
      System.out.println(inputString);
    }while(!inputString.equals("q"));

    System.out.println();
    System.out.println("프로그램 종료");
  }
}
```