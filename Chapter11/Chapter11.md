# Chapter11 기본 API 클래스

## 11.1 자바 API 도큐먼트



**API**

- 자바에서 기본적으로 제공하는 라이브러리(library)
- 프로그래밍 개발에 자주 사용되는 클래스 및 인터페이스 모음



**API 도큐먼트**

- 쉽게 API를 찾아 이용할 수 있도록 문서화한 것
- https://docs.oracle.com/javase/8/docs/api/



![Alt text](자바API도큐먼트.PNG)



<br/>

---

## 11.2 java.lang과 java.util 패키지



### 11.2.1 java.lang 패키지

- 자바 프로그램의 기본적인 클래스를 담은 패키지
- java.lang 패키지에 있는 클래스와 인터페이스는 import 없이 사용 가능

![Alt text](javalang.PNG)



### 11.2.2 java.util 패키지

- 컬렉션 클래스들이 대부분

| 클래스          | 용도                                          |
| :-------------- | :-------------------------------------------- |
| Arrays          | 배열을 조작(비교, 복사, 정렬, 찾기)할 때 사용 |
| Calender        | 운영체제의 날짜와 시간을 얻을 때 사용         |
| Date            | 날짜와 시간 정보를 저장하는 클래스            |
| Objects         | 객체비교, null 여부 등을 조사할 때 사용       |
| StringTokenizer | 특정 문자로 구분된 문자열을 뽑아낼 때 사용    |
| Random          | 난수를 얻을 때 사용                           |



<br/>

---

## 11.3 Object 클래스

- 자바의 최상위 부모 클래스
- extends 키워드로 다른 클래스를 상속하지 않으면 암시적으로 java.lang.Object 클래스 상속
- 모든 클래스가 Object를 상속하기 때문에 모든 클래스에서 사용 가능



### 11.3.1 객체 비교(equals)

```java
public boolean equals(Object obj){...}
```

equals()의 매개 타입은 Object인데, 이것은 모든 객체가 매개값으로 대입될 수 있음을 말한다. 그 이유는 Object가 취상위 타입이므로 모든 객체는 Object 타입으로 자동 타입 변환될 수 있기 때문이다.

Object 클래스의 equals()는 비교 연산자인 ==과 동일한 결과를 리턴한다. **하지만 ==는 두 객체의 주소값을 비교**하지만 **equals()는 객체를 논리적으로 비교하여 같은 객체이건 다른 객체이건 상관없이 객체가 저장하고 있는 값을 비교**한다.

equals()를 재정의할 때에는 매개값(비교 개체)이 기준 객체와 동일한 타입의 객체인지 먼저 확인해야 한다. Object 타입의 매개 변수는 모든 객체가 매개값으로 제공될 수 있기 때문에 **instanceof** 연산자로 기준 객체와 동일한 타입인지 제일 먼저 확인해야 한다.

![Alt text](equals.PNG)

![Alt text](equalsMain.PNG)



### 11.3.2 객체 해시코드(hashCode())

객체 해시코드란 객체를 식별할 수 있는 하나의 정수값을 말한다. Object의 hashCode()는 **객체의 메모리 번지를 이용해서 해시코드를 만들어 리턴**하기 때문에 **객체마다 다른 값을 가지고 있다**.

논리적 동등 비교 시 hashCode()를 오버라이딩할 필요성이 있다. 해시코드 값이 다르면 다른 객체, 같으면 equals()로 다시 비교한다. 그래서 **hashCode()가 true가 나와도 equals()의 리턴값이 다르면 다른 객체가 된다.**

![Alt text](해시코드.PNG)



![Alt text](HashCode.PNG)

![Alt text](HashCodeMain.PNG)



### 11.3.3 객체 문자 정보(toString())

Object 클래스의 toString()은 객체의 문자 정보를 리턴한다. 객체의 문자 정보란 객체를 문자열로 표현한 값을 말한다. 기본적으로 Object의 toString()은 "클래스명@16진수 해시코드"로 구성된 문자 정보를 리턴한다.



![Alt text](ToString.PNG)

![Alt text](ToStringMain.PNG)



### 11.3.4 객체 복제(clone())

객체 복제는 원본 객체의 필드값과 동일한 값을 가지는 새로운 객체를 생성하는 것을 말한다. 객체를 복제하는 이유는 **원본 객체를 안전하게 보호**하기 위해서 이다.

- 얕은 복제(thin clone)

  - 필드 값만 복제

  - 필드가 기본 타입일 경우 값 복사가 일어나고, 필드가 참조 타입일 경우 객체의 번지가 복사된다.

    ![Alt text](얕은복제.PNG)

  - Object의 clone 메소드는 자신과 동일한 필드값을 가진 얕은 복제된 객체를 리턴

  - 이 메소드를 사용하기 위해서 원본 객체는 반드시 java.lang.Cloneable 인터페이스 구현 -> 메소드 선언이 없음에도 불구하고 Cloneable 인터페이스를 명시적으로 구현하는 이유는 설계짜가 복제를 허용한다는 의도적인 표시

  - clone()메소드를 호출할 때 CloneNotSupportedException 발생, try-catch 필요

![Alt text](thinClone.PNG)

![Alt text](thinCloneMain.PNG)



- 깊은 복제(deep clone)

  - 얕은 복제는 참조 타입의 필드는 번지만 복제되기 때문에 원본 객체와 복제 객체가 같은 객체를 참조

  - 만약 복제 객체에서 참조 타입의 필드 값을 변경하면 원본 객체의 참조 타입 필드 값도 변경 -> 얕은 복제의 단점

  - 깊은 복제는 **참조하고 있는 객체도 복제**하는 것

    ![Alt text](깊은복제.PNG)

  - 깊은 복제를 하려면 Object의 clone()를 재정의해서 참조 객체를 복제하는 코드를 직접 작성



![Alt text](DeepClone.PNG)

![Alt text](Car.PNG)

![Alt text](DeepCloneMain.PNG)

### 11.3.5 객체 소멸자(finalize())

참조하지 않은 배열이나 객체는 쓰레기 수집기가 힙 영역에서 자동적으로 소멸시킨다. 쓰레기 수집기는 객체를 소멸하기 직전에 마지막으로 객체의 소멸자(finalize())를 실행시킨다.

기본적으로 finalize()는 실행 내용이 없다. 만약 객체가 소멸되기 전에 마지막으로 사용했던 자원을 닫고 싶거나 중요한 데이터를 저장하고 싶다면 Object의 finalize()를 재정의할 수 있다.

![Alt text](finalize.PNG)

![Alt text](finalizeMain.PNG)